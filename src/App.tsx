import React from "react";
import Landing from "./components/routes/Landing";

const App = () => {
  return <Landing />;
};

export default App;
