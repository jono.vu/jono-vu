import React from "react";

import { sections } from "../config";
import { Section } from "../Section";
import Layout from "../Layout";
import { About } from "./About";

const Landing = () => {
  return (
    <Layout>
      <div className="bg-white pt-16 pb-20 px-4 sm:px-6 lg:pt-24 lg:pb-28 lg:px-8">
        {/* <div className="relative max-w-lg mx-auto lg:max-w-7xl"> */}
        <About />
        <div>
          {sections.map(section => {
            return <Section section={section} />;
          })}
        </div>
      </div>
    </Layout>
  );
};

export default Landing;
