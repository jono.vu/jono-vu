import React from "react";

import Paragraph from "../ui/Paragraph";

export const About = () => {
  return (
    <div>
      <h2 className="text-3xl leading-9 tracking-tight font-extrabold text-gray-900 sm:text-4xl sm:leading-10">
        Jonathan Vu
      </h2>
      <Paragraph>
        Jonathan Vu | Designer | Developer. jono.vu@gmail.com.
      </Paragraph>
      <p className="mt-3 text-xl leading-7 text-gray-500 sm:mt-4">
        I design, code & analyse digital user experiences with a focus on
        business & marketing strategy. For the last 5 years, I studied design
        and consulted with small to medium scale businesses to improve their
        digital potential and realise their aesthetic visions. Exploring new
        technologies, developing user-driven design & creating value are what I
        aim to achieve with my practice. I design, code & analyse digital user
        experiences with a focus on business & marketing strategy. For the last
        5 years, I studied design and consulted with small to medium scale
        businesses to improve their digital potential and realise their
        aesthetic visions. Exploring new technologies, developing user-driven
        design & creating value are what I aim to achieve with my practice.
      </p>
    </div>
  );
};
