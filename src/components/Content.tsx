import React from "react";

import { ContentType } from "./types";

import Badge from "./ui/Badge";
import Heading from "./ui/Heading";
import Paragraph from "./ui/Paragraph";
import Anchor from "./ui/Anchor";
import Block from "./ui/Block";

export const Content: React.FC<{ content?: ContentType }> = ({ content }) => {
  if (!content) return null;
  const { title, subTitle, description, link } = content;
  return (
    <Block>
      <Heading>{title}</Heading>
      <Badge>{subTitle}</Badge>
      <Paragraph>{description}</Paragraph>
      {link && <Anchor href={link}>{link}</Anchor>}
    </Block>
  );
};
