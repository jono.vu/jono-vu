import { status, SectionType } from "./types";

const designFolioSection = {
  label: "Design Folio",
  content: {
    title: "Design Folio available here:",
    subTitle: null,
    description: null,
    link: "jonovu.github.io"
  }
};
const currentWorkplaceSection = {
  label: "Current Workplace",
  content: {
    title: "Currently @Momentium Consulting",
    subTitle: "Front-end Developer",
    description:
      "Improving UX & design for businesses looking to grow their digital capabilities. Introducing modern technologies for agile web development. Offering both high level and detail-oriented, user driven solutions that best carry business values and represent brand strategy.",
    link: "https://www.momentium.com.au"
  }
};
const projectsSection = {
  label: "Projects",
  projects: [
    {
      name: "Riverside Creative",
      status: status.incomplete,
      description: "",
      link: "https://riverside-creative.netlify.app/"
    },
    {
      name: "Renov'Project",
      status: status.incomplete,
      description: "",
      link: "https://renov-project.netlify.app/"
    },
    {
      name: "Taylor Made Eatery",
      status: status.complete,
      description: "",
      link: "http://taylor-made.com.au/"
    },
    {
      name: "Mmucho",
      status: status.incomplete,
      description: "",
      link: "https://mmucho-preview.netlify.app/"
    }
  ]
};
const personalsSection = {
  label: "Personal Projects",
  projects: [
    {
      name: "Mount Film",
      status: status.complete,
      description: "",
      link: "https://mount-film.netlify.app"
    },
    {
      name: "Chord Name",
      status: status.incomplete,
      description: "",
      link: "https://renov-project.netlify.app/"
    },
    {
      name: "pong.js",
      status: status.complete,
      description: "",
      link: "http://jon-pong.com.au/"
    }
  ]
};

export const sections: SectionType[] = [
  designFolioSection,
  currentWorkplaceSection,
  projectsSection,
  personalsSection
];
