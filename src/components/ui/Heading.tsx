import React from "react";

const Heading: React.FC<{ children: React.ReactNode | string | null }> = ({
  children
}) => {
  return (
    <h3 className="mt-4 text-xl leading-7 font-semibold text-gray-900">
      {children}
    </h3>
  );
};

export default Heading;
