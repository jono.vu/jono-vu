import React from "react";

const Badge: React.FC<{ children: React.ReactNode | string | null }> = ({
  children
}) => {
  return (
    <div className="inline-block">
      <span className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium leading-5 bg-indigo-100 text-indigo-800">
        {children}
      </span>
    </div>
  );
};

export default Badge;
