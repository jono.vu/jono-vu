import React from "react";

interface Props {
  href: string;
  children: React.ReactNode | string | null;
}

const Anchor: React.FC<Props> = ({ href, children }) => {
  return (
    <a
      className="text-base leading-6 font-semibold text-indigo-600 hover:text-indigo-500 transition ease-in-out duration-150"
      href={href}
      target="blank"
      rel="noreferrer nooponer"
    >
      {children}
    </a>
  );
};

export default Anchor;
