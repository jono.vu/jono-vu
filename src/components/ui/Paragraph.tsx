import React from "react";

const Paragraph: React.FC<{ children: React.ReactNode | string | null }> = ({
  children
}) => {
  return <p className="mt-3 text-base leading-6 text-gray-500">{children}</p>;
};

export default Paragraph;
