import React from "react";

const Block: React.FC<{ children: React.ReactNode | string | null }> = ({
  children
}) => {
  return <div className="block">{children}</div>;
};

export default Block;
