import React from "react";

import { ProjectType } from "./types";
import Heading from "./ui/Heading";
import Badge from "./ui/Badge";
import Paragraph from "./ui/Paragraph";
import Anchor from "./ui/Anchor";
import Block from "./ui/Block";

export const Project: React.FC<{ project?: ProjectType }> = ({ project }) => {
  if (!project) return null;
  const { name, status, link, description } = project;
  return (
    <Block>
      <Heading>{name}</Heading>
      <Badge>{status}</Badge>
      <Paragraph>{description}</Paragraph>
      {link && <Anchor href={link}>{link}</Anchor>}
    </Block>
  );
};
