import React from "react";

import { SectionType } from "./types";

import { Projects } from "./Projects";
import { Content } from "./Content";

export const Section: React.FC<{ section: SectionType }> = ({ section }) => {
  // const { label } = section;
  return (
    <div>
      {/* <h3>{label}</h3> */}
      <Content content={section.content} />
      <Projects projects={section.projects} />
    </div>
  );
};
