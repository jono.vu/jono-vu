export enum status {
  incomplete = "DEVELOPMENT",
  complete = "LIVE"
}

export type SectionType = {
  label: string;
  content?: ContentType;
  projects?: ProjectType[];
};

export type ContentType = {
  title: string | null;
  subTitle: string | null;
  description: string | null;
  link: string | null;
};

export type ProjectType = {
  name: string | null;
  status: string | null;
  link: string | null;
  description: string | null;
};
