import React from "react";

import { ProjectType } from "./types";

import { Project } from "./Project";

export const Projects: React.FC<{ projects?: ProjectType[] }> = ({
  projects
}) => {
  if (!projects) return null;
  return (
    <div className="mt-12 grid gap-16 border-t-2 border-gray-100 pt-12 lg:grid-cols-3 lg:col-gap-5 lg:row-gap-12">
      {projects.map(project => {
        return <Project project={project} />;
      })}
    </div>
  );
};
