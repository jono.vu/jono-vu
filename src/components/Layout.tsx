import React from "react";

import "../tailwind.output.css";

const Layout: React.FC<{ children: React.ReactNode | string | null }> = ({
  children
}) => {
  return (
    <div style={{ width: "100vw" }}>
      <header />
      <div>{children}</div>
      <footer />
    </div>
  );
};

export default Layout;
